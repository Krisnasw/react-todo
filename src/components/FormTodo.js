import React, { Component } from 'react'
import {
    Form, Input, Card, Button
} from 'antd';
import { connect } from 'react-redux'
import { addTodo } from '../actions/todo'
import { bindActionCreators } from 'redux';

class FormTodo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            todoTitle: '',
            todoDesc: ''
        }

        this.onChangeTodoTitle = this.onChangeTodoTitle.bind(this)
        this.onChangeTodoDesc = this.onChangeTodoDesc.bind(this)
    }

    onChangeTodoTitle(e) {
        this.setState({
            todoTitle: e.target.value
        })
    }

    onChangeTodoDesc(e) {
        this.setState({
            todoDesc: e.target.value
        })
    }

    onFormReset = () => {
        this.setState({
            todoTitle: '',
            todoDesc: ''
        })
    }

    render() {
        return (
            <Card
                title="Buat Tugas Baru"
            >
                <Form>
                    <Form.Item
                        label="Judul Tugas"     
                    >
                        <Input type="text" placeholder="Masukkan Judul Tugas" onChange={this.onChangeTodoTitle} value={this.state.todoTitle} />
                    </Form.Item>
                    <Form.Item
                        label="Deskripsi Tugas"
                    >
                        <Input type="text" placeholder="Deskripsi Tugas" onChange={this.onChangeTodoDesc} value={this.state.todoDesc} />
                    </Form.Item>
                    <Button type="primary" onClick={ () => { this.props.addTodo(this.state.todoTitle, this.state.todoDesc); this.setState({ todoTitle: '', todoDesc: '' }) }}> Submit </Button>
                    <Button type="cancel" style={{ marginLeft: 10 }} onClick={this.onFormReset}> Cancel </Button>
                </Form>
            </Card>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        addTodo
    }, dispatch)
}

export default connect(null, mapDispatchToProps)(FormTodo)
