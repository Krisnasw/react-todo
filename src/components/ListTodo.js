import React, { Component } from 'react'
import { connect  } from 'react-redux'
import {
    editTodo,
    deleteTodo,
    showTodo,
    setVisibilityFilter
} from '../actions/todo'
import { SHOW_ALL, SHOW_COMPLETED, SHOW_ACTIVE } from "../constants/config"
import { bindActionCreators } from 'redux'
import {
    Empty, Breadcrumb, Button, Row, Col, Divider, Modal, Form, Input
} from 'antd'
import styled from 'styled-components'

class ListTodo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalVisible: false,
            titleEdit: '',
            contentEdit: ''
        }
        this.handleClose = this.handleClose.bind(this)
        this.handleTitleEdit = this.handleTitleEdit.bind(this)
        this.handleContentEdit = this.handleContentEdit.bind(this)
    }

    showModal = () => {
        this.setState({
          modalVisible: true,
        });
    }

    handleClose() {
        this.setState({ modalVisible: false })
    }

    handleTitleEdit(e) {
        this.setState({
            titleEdit: e.target.value
        })
    }

    handleContentEdit(e) {
        this.setState({
            contentEdit: e.target.value
        })
    }

    render() {
        const Text = styled.p `
            color: black;
            font-weight: bold;
        `;

        const ContentTodo = styled.p `
            color: grey;
        `;

        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item onClick={() => this.props.setVisibilityFilter(SHOW_ALL)}>All</Breadcrumb.Item>
                    <Breadcrumb.Item onClick={() => this.props.setVisibilityFilter(SHOW_COMPLETED)}>Completed</Breadcrumb.Item>
                    <Breadcrumb.Item onClick={() => this.props.setVisibilityFilter(SHOW_ACTIVE)}>Active</Breadcrumb.Item>
                </Breadcrumb>
                {this.props.todos.length !== 0 ? (
                    <div>
                        {this.props.todos.map( (todo, index) => (
                            <div key={index}>
                                <Divider />
                                <Row>
                                    <Col span={6}>
                                        <Text>{todo.title} - {todo.completed === true ? "Completed" : "Progress"}</Text>
                                        <ContentTodo style={{ marginTop: -10 }}>{todo.content}</ContentTodo>
                                    </Col>
                                    <Col span={3}>
                                        <Button type="primary" icon="check" onClick={ () => this.props.showTodo(todo.id) }>
                                            Completed
                                        </Button>
                                    </Col>
                                    <Col span={3}>
                                        <Button type="info" icon="edit" onClick={this.showModal}>
                                            Edit
                                        </Button>
                                    </Col>
                                    <Col span={3}>
                                        <Button type="danger" icon="delete" onClick={ () => this.props.deleteTodo(todo.id) }>
                                            Delete
                                        </Button>
                                    </Col>
                                </Row>
                                <Modal
                                    title={"Edit Data - "+todo.title}
                                    visible={this.state.modalVisible}
                                    onOk={ () => { this.props.editTodo(todo.id, this.state.titleEdit, this.state.contentEdit); this.setState({ modalVisible: false, titleEdit: '', contentEdit: '' }) } }
                                    onCancel={this.handleClose}
                                >
                                    <Form>
                                        <Form.Item
                                            label="Judul Tugas"
                                        >
                                            <Input placeholder={todo.title} onChange={this.handleTitleEdit} />
                                        </Form.Item>
                                        <Form.Item
                                            label="Deskripsi Tugas"
                                        >
                                            <Input placeholder={todo.content} onChange={this.handleContentEdit} />
                                        </Form.Item>
                                    </Form>
                                </Modal>
                            </div>
                        ))}
                    </div>
                ) : (
                    <Empty />
                )}{" "}
            </div>
        );
    }
}

const getVisibleTodos = (todos, filter) => {
    switch (filter) {
      case SHOW_ALL:
        return todos;
      case SHOW_COMPLETED:
        return todos.filter(t => t.completed);
      case SHOW_ACTIVE:
        return todos.filter(t => !t.completed);
      default:
        throw new Error("Error: " + filter);
    }
};

const mapStateToProps = state => {
    return { todos: getVisibleTodos(state.todos, state.visibilityFilter),
        visibilityFilter: state.visibilityFilter
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            editTodo,
            deleteTodo,
            showTodo,
            setVisibilityFilter
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ListTodo)
