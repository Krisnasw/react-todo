import React from 'react'
import {
    Modal
} from 'antd'

const editModal = ({ closeModal, title, message }) => {
    return (
        <div>
            <Form>
                <Form.Item
                    label="Judul Tugas"     
                >
                    <Input type="text" placeholder="Masukkan Judul Tugas" />
                </Form.Item>
                <Form.Item
                    label="Deskripsi Tugas"
                >
                    <Input type="text" placeholder="Deskripsi Tugas" />
                </Form.Item>
                <Button type="primary"> Submit </Button>
                <Button type="cancel"> Cancel </Button>
            </Form>
        </div>    
    );
}

export default editModal