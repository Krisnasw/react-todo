import React, { Component } from 'react';
import { Layout, Menu, Breadcrumb, Card } from 'antd'
import FormTodo from './FormTodo'
import ListTodo from './ListTodo'
import '../App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: false
        }
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    render() {
        const { Header, Content } = Layout;
        return (
            <Layout style={{ height: "100vh" }}>
                <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['1']}
                    style={{ lineHeight: '64px' }}
                >
                    <Menu.Item key="1">Home</Menu.Item>
                </Menu>
                </Header>
                <Content style={{ padding: '0 50px', marginTop: 64, height: '100vh' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                    <Breadcrumb.Item>List Todo</Breadcrumb.Item>
                </Breadcrumb>
                <FormTodo />
                <Content style={{ marginTop: 16 }} />
                <Card
                    title="List Todo"
                    >
                    <ListTodo />
                </Card>
                </Content>
            </Layout>
        );
    }
}

export default App
