import { ADD_TODO, EDIT_TODO, REMOVE_TODO, TOGGLE_TODO } from '../constants/config'

const INITIAL_DATA = []

const TodoReducers = (state = INITIAL_DATA, action) => {
    switch (action.type) {
        case ADD_TODO:
            return [
                ...state, {
                    id: action.id,
                    title: action.title,
                    content: action.content,
                    completed: false
                }
            ]
        case EDIT_TODO:
            return state.map(todo => (todo.id === action.id ? {...todo, ...action} : todo))
        case TOGGLE_TODO:
            return state.map(todo => (todo.id === action.id) ? { ...todo, completed: !todo.completed } : todo)
        case REMOVE_TODO:
            const numIndex = parseInt(action.id)
            return state.filter(todo => todo.id !== numIndex);
        default:
            return state
    }
}

export default TodoReducers;
