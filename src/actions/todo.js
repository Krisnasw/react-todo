import { ADD_TODO, EDIT_TODO, REMOVE_TODO, TOGGLE_TODO, SET_VISIBILITY_FILTER } from '../constants/config'

let TodoId = 1

export const addTodo = (title, content) => ({
    type: ADD_TODO,
    id: TodoId++,
    title,
    content
})

export const editTodo = (id, title, content) => ({
    type: EDIT_TODO,
    id,
    title,
    content
})

export const deleteTodo = (id) => ({
    type: REMOVE_TODO,
    id: id
})

export const showTodo = (id) => ({
    type: TOGGLE_TODO,
    id: id
})

export const setVisibilityFilter = filter => ({
    type: SET_VISIBILITY_FILTER,
    filter
})
